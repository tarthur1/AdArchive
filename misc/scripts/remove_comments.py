# This script removes all oxygen comments from a given xml file. Note: it will also remove changes that have been tracked
# but not yet commited.

# import modules
import xml.etree.ElementTree as ET

# open and parse xml file
tree = ET.parse('test.xml')
root = tree.getroot()

# remove void elements
for elem in root.iter():
   if not elem.text and not elem.tail:
        parent = elem.getparent()
        parent.remove(elem)

# save xml file
tree.write('test.xml')
