# Generates a csv of unique @uri attribute values for ad creators from a directory of XML files
from lxml import etree
import os
import sys
import csv
import numpy as np
import pandas as pd

#create a dataframe to hold the data
df = pd.DataFrame(columns=['uri', 'names', 'count', 'issues', 'clippings'])
creators_count = {}
issues = {}
names = {}
clippings = {}

parser = etree.XMLParser(remove_blank_text=True, remove_comments=True)

# Get source directory from argument
try:
    source_dir = sys.argv[1]
except:
    source_dir = '/Users/tim/Local/AdArchive/xml_3m/publication_data/heresies/xml_issues/with_ids'
    print(f'No source directory specified. Assuming heresies data from {source_dir}.')

# Add unique @uri values from an XML file to a dictionary
def parse(xml_file_name):
    try:
        xml_file = open(f'{source_dir}/{xml_file_name}', 'r')
        tree = etree.parse(xml_file, parser)
    except Exception as e:
        print(f'Error parsing {xml_file}.\nError: {e}')
    return tree

# function to count creators and add them to the dataframe
def count_and_label_creators(root):
    creators = root.xpath('//ad_creators/creator')
    for creator in creators:
        creator_uri = creator.get('uri')
        creators_count[creator_uri] = int(creators_count.get(creator_uri, 0) + 1)
        issue = creator.xpath('./ancestor::issue/number/text()')[0]
        issues[creator_uri] = issues.get(creator_uri, []) + [issue]
        creator_name = creator.xpath('name/text()')[0]
        names[creator_uri] = names.get(creator_uri, []) + [creator_name]
        clipping = creator.xpath('./ancestor::ad/@clipping_uri')[0]
        clippings[creator_uri] = clippings.get(creator_uri, []) + [clipping]
        # add creators to the dataframe
        df.loc[creator_uri] = (creator_uri, set(names[creator_uri]), creators_count[creator_uri], set(issues[creator_uri]), set(clippings[creator_uri]))


# Iterate through a directory of XML files and parse them
try:
    for file_name in os.listdir(source_dir):
        if file_name.endswith('.xml') & file_name.startswith('heresies'):
            print(f'Processing {file_name}...')
            tree = parse(file_name)
            root = tree.getroot()
            count_and_label_creators(root)
except Exception as e:
    print(f'Error processing {file_name}.\nError: {e}')

# Write the dataframe to a csv
out_path = './misc/ad_creators.csv'
print(f'Writing {out_path} ...')
try:
    with open(out_path, 'w') as csvfile:
        df.to_csv(csvfile, index=False)
except Exception as e:
    print(f'Error writing {out_path}.\nError: {e}')

