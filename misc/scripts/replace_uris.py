import rdflib, csv

g = rdflib.Graph()

g.parse("3m-transformed-output.ttl", format="ttl")

n = rdflib.Graph()

replacement_uris = {}
replacement_labels = {}

for s, p, o in g:
	s = replacement_uris.get(s, s)
	o = replacement_uris.get(o, o)
	n.add((s, p, o))

n.serialize(destination='replaced.ttl', format='ttl' )
