import rdflib, csv

g = rdflib.Graph()

g.parse("3m-transformed-output.ttl", format="n3")

n = rdflib.Graph()


literal_uris = {}
replacement_uris = {}

for s,p,o in g:
    if isinstance(o, rdflib.term.Literal):
        if not o in literal_uris:
            literal_uris[o] = s
        else:
            replacement_uris[s] = literal_uris[o]

for s, p, o in g:
	s = replacement_uris.get(s, s)
	o = replacement_uris.get(o, o)
	n.add((s, p, o))

n.serialize(destination='clustered.ttl', format='ttl' )
