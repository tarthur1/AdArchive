# Generates a csv of unique @uri attribute values for ad creators from a directory of XML files
from lxml import etree
import os
import sys
import csv

global creators_count
creators_count = {}
global creators_labels
creators_labels = {}

parser = etree.XMLParser(remove_blank_text=True, remove_comments=True)

# Get source directory from argument
try:
    source_dir = sys.argv[1]
except:
    source_dir = '/Users/tim/Local/AdArchive/xml_3m/publication_data/heresies/xml_issues/with_ids'
    print(f'No source directory specified. Assuming heresies data from {source_dir}.')

# Add unique @uri values from an XML file to a dictionary
def parse(xml_file_name):
    try:
        xml_file = open(f'{source_dir}/{xml_file_name}', 'r')
        tree = etree.parse(xml_file, parser)
    except Exception as e:
        print(f'Error parsing {xml_file}.\nError: {e}')
    return tree

# function to count creators
def count_and_label_creators(root):
    creators = root.xpath('//ad_creators/creator')
    for creator in creators:
        creator_uri = creator.get('uri')
        creators_count[creator_uri] = creators_count.get(creator, 0) + 1
        creator_label = creator.find('name').text
        print(creator_label)
        creators_labels[creator_uri] = creator_label

# Iterate through a directory of XML files and parse them
try:
    for file_name in os.listdir(source_dir):
        if file_name.endswith('.xml'):
            print(f'Processing {file_name}...')
            tree = parse(file_name)
            root = tree.getroot()
            count_and_label_creators(root)
except Exception as e:
    print(f'Error processing {file_name}.\nError: {e}')

# Write the dictionary to a csv file
out_path = './misc/ad_creators.csv'
print(f'Writing {out_path} ...')
try:
    with open(out_path, 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['creator', 'uri', 'count'])
        for key, value in creators_count.items():
            writer.writerow([creators_labels[key], key, value, ])

except Exception as e:
    print(f'Error writing uris.csv.\nError: {e}')

