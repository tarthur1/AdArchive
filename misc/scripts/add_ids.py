import sys
# the lxml version of etree is used as it allows accessing parent elements directly from child elements
from lxml import etree
import pprint
import re
import os
from pathlib import Path
from unidecode import unidecode
import traceback

# extracting the publication name from the command line
global publication
try:
    publication = sys.argv[1]
except:
    publication = 'Heresies'
    print(f'No publication name provided. Using default value: "{publication}"')

dir_path=f'./xml_3m/publication_data/{publication.lower()}/xml_issues/comments_removed'
print(f'Adding ids to *.xml in {dir_path}\n')

pp = pprint.PrettyPrinter(indent=4)

try:
    validate = sys.argv[2]
except:
    validate = False

if validate:
    schema = etree.XMLSchema(etree.parse('./xml_3m/adarchive_schema.xsd'))
    parser = etree.XMLParser(remove_comments=True, attribute_defaults=True, schema=schema)
else:
    parser = etree.XMLParser(remove_comments=True, attribute_defaults=True)

global def_prefix
def_prefix = 'http://temp.lincsproject.ca/'

def to_string(element: etree._Element) -> str:
    string = etree.tostring(element, pretty_print=True)
    return string

def get_label(element: etree._Element) -> str:
    # text = ''
    # if element.tag in about_tags:
    #     target = get_about_target(element)
    #     if target is not None:
    #         element = target
    #     #print(f'About detected: {to_string(element)}')
    # if element.text is not None and element.text.strip() != '':
    #     text = element.text
    # else:
    #     try:
    #         titles = element.xpath(".//title")
    #         names = element.xpath(".//name|.//event_name|.//address")
    #         if titles:
    #             text = titles[0].text
    #         elif names:
    #             text = names[0].text
    #     except:
    #         print(f'No text found for element {etree.tostring(element, pretty_print=True)}')
    # if text == '' or text is None:
    #     print(f'No label found for element:{to_string(element)}\n')
    # return text
    element_label = gen_id(element, True)
    if element_label == None:
        print("Error: Missing label")
    return element_label

# function to clean up the id strings, replacing special characters and any whitespaces, and making them uppercase
def normalize(string: str, to_uppercase=True) -> str:
    if string.lower().startswith('http'):
        string = string.split('/')[-1]
    string = unidecode(string)
    string = string.strip()
    string = re.sub(' +', ' ', string)
    string = re.sub("[\W_]+", "_", string)
    # remove leading and trailing underscores
    if string.startswith('_'):
        string = string[1:]
    if string.endswith('_'):
        string = string[:-1]
    if to_uppercase:
        string = string.upper()

    return string

def append_numeric_id(element: etree._Element, element_id: str, label: bool = False) -> str:
    #append numeric id if element does not end with a number
    if element_id[-1].isdigit():
        return element_id
    else:
        element_number = element.get('count')
        if not element_number:
            if id_counts.get(element_id):
                id_counts[element_id] += 1
                element_number = id_counts[element_id]
            else:
                element_number = 1
        else:
            element_number = int(element_number)
        
        id_counts[element_id] = element_number
        element.set('count', str(id_counts[element_id]))
        element_id += f" {element_number}"
        if not label:
            element_id = normalize(element_id)
    return element_id

def gen_generic_id(element: etree._Element, label:bool=False) -> str:
    id = f"{publication} {issue_number} {element.tag}"
    id = append_numeric_id(element, id, label)
    if not label:
        id = normalize(id)
    return id

def gen_ad_label_id(element: etree._Element, label:bool=False) -> str:
    ads = element.xpath('./ancestor-or-self::ad') 
    ad = None
    if ads:
        ad = ads[0]
    else:
        element_label = gen_generic_id(element, True)
    if ad is not None:
        try:
            ad_label = gen_ad_id(ad, True)
            element_label = f"{ad_label} {element.tag}"
            element_label = append_numeric_id(element, element_label, label)
        except:
            print(f"No ad label found for {to_string(element)[0:100]} Generating generic ID")
            element_label = gen_generic_id(element, True)
    if label:
        element_id = element_label
    else:
        element_id = normalize(element_label)
    #print(f'Generated generic ID {element_id} for {element.tag} {element.text}')
    return element_id

def gen_pub_id(element: etree._Element, label:bool=False) -> str:
    title = element.find("./title")
    author = element.find("./creator/name")
    pub_type = element.find("./publication_type")
    about = element.find("./about")
    element_label = ''
    if not title == None:
        element_label = title.text
    else:
        if not pub_type == None:
            element_label = f'Untitled {pub_type.text}'
        else:
            element_label = f"Untitled {element.tag}"

        if not author == None:
            element_label += f" by {author.text}"

        if not about == None:
            element_label += f" about {get_label(about)}"

    if not (element_label == ''):
        element_label = clean_label(element_label)
    else:
        content = etree.tostring(element, pretty_print=True)
        print(f"Publication information not found for {element} with content:\n{content}\nGenerating generic ID.")
        element_label = gen_ad_label_id(element, label)

    element_label = clean_label(element_label)
    element.text = element_label

    if label:
        element_id = element_label
    #TODO Fix "Expression" in labels where it is not needed
    else:
        element_id = normalize(element_label) + "_EXPRESSION"
    
    # if label:
    #     print(f'Generated label {element_id} for publication')
    # else:
    #     print(f'Generated ID {element_id} for publication')
    
    return element_id

def gen_serial_id(element: etree._Element, label:bool=False) -> str:
    type = 'issue'
    if element.tag == 'volume':
        type = 'vol'

    publications = element.xpath("./ancestor::publication")
    publication = publications[-1]
    publication_title = publication.find("./title")
    element_label = 'Untitled publication'
    try:
        element_label = publication_title.text
    except:
        print(f"DATA ERROR: Publication title not found for {element.tag} {element.text}. Generating generic ID.")
        print(f"element: {to_string(element)}")
        print(f"publication_title: {publication_title}")
        element_id = gen_ad_label_id(element, label)

    try:
        serial_title = element.find("./title")
        serial_number = element.find("./number")
        serial_date = element.find("./date")
        volume_number = None
        append = ''
        # get the volume number, if any, if the element is an issue
        # then append the volume number and either the issue number, issue date, or issue title (in order of priority) to the id
        if type == 'issue':
            volumes = element.xpath("./ancestor::volume")
            if not volumes == []:
                volume = volumes[-1]
                volume_number = volume.find("./number")
                if not volume_number == None:
                    append = ' Vol. ' + volume_number.text
        if not serial_number == None:
            append += f' {type} {serial_number.text}'
        elif not serial_date == None:
            date_text = serial_date.find("./text")
            if date_text == None:
                date_text = serial_date.text
                print(f"DATA ERROR: No date/text element found for {element.tag} {element.text}. Using text of date element: {date_text}")
            append += f' {type} {serial_date.text}'
        elif not serial_title == None:
            append += f' {type} {serial_title.text}'
        #print(f"Append: {append}")
        element_label += append
    except Exception as e:
        print(f"No serial title, number or date found for {element.tag} {element.text}. Generating generic ID.\nError: {e}")
        element_label = gen_ad_label_id(element, label)

    element_label = clean_label(element_label)
    element.text = element_label

    if label:
        element_id = element_label
    else:
        element_id = normalize(element_label)

    return element_id

def gen_ad_id(element: etree._Element, label:bool=False):
    # find child with @is_main_subject = True
    main_subject = element.find("./about[@is_main_subject='true']")
    parent_subject = element.find("../about[@is_main_subject='true']")
    if main_subject == None:
        if element.get("count") is None:
            print(f"DATA ERROR: No main subject found for {element.tag} {element.get("clipping_uri")}. Generating generic ID.")
        ad_label = gen_generic_id(element, label=True)
    elif main_subject[0].text==None:
        if element.get("count") is None:
            print(f"DATA ERROR: Main subject missing label for {element.tag} {element.get("clipping_uri")}. Generating generic ID.")
        ad_label = gen_generic_id(element, label=True)
    elif parent_subject == None:
        ad_label = f"{publication} {issue_number} '{get_label(main_subject)}' Ad"
    else:
        ad_label = f"{publication} {issue_number} '{get_label(parent_subject)}' Ad - '{get_label(main_subject)}' Embedded Ad"
    element_label = clean_label(ad_label)
    element.text = element_label

    if label:
        element_id = element_label
    else:
        element_id = normalize(element_label)
    
    return element_id

def gen_id_from_text(element: etree._Element, label:bool=False)->str:
    if not ((element.text==None) | (element.text=="")):
        element_id = " ".join(element.text.split()[:15])
    else:
        #print(f"The element {to_string(element)} has no text. Generating generic ID.")
        element_id = gen_ad_label_id(element, label)

    if not label:
        element_id = normalize(element_id)
    return element_id

#TODO fix doubled tag in ID, tag in label for title
def gen_id_from_text_and_tag(element: etree._Element, label:bool=False)->str:
    text = element.text
    tag = element.tag
    if not ((text==None) | (text=="")):
        element_id = gen_id_from_text(element, label)
    else:
        print(f"The element {to_string(element)} has no text. Generating generic ID.")
        element_id = gen_ad_label_id(element, label)
        return element_id
    
    if not (text.endswith(tag) | label):
        element_id = f"{text} {tag}"

    if not label:
        element_id = normalize(element_id)

    #print(f'Generated ID {element_id} for:\n{to_string(element)}')

    return element_id

def gen_location_id(element: etree._Element, label=False)->str:
    try:
        coordinates = element.find('./coordinates')
        name = element.find('./name')
        address = element.find('./address')
        if name is not None:
            element_label = f"{name.text} location"
        elif address is not None:
            element_label = f"{address.text} location"
        elif coordinates is not None:
            element_label = f"{coordinates.text} location"
        else:
            element_label = "Unspecified location"
    except Exception as e:
        print(f'Location {to_string(element)} is missing coordinates and name')
    
    element_label = clean_label(element_label)
    element.text = element_label
    if label:
        element_id = element_label
    else:
        element_id = normalize(element_label)
    return element_id

def gen_creator_id(element: etree._Element, label:bool=False)->str:
    name = element.find("./name")
    if name==None:
        element_id = element.text
    else:
        element_id = name.text

    if element_id == None:
        element_uri = element.get('uri')
        if element_uri is not None:
            message = element_uri
        else:
            message = to_string(element)[:200]
        print(f"DATA ERROR: No name found for creator element: {message}")
        element_id = "Unnamed creator"
    
    if not label:
        element_id = normalize(element_id)

    return element_id

def get_about_target(element: etree._Element)->etree._Element:
    try:
        children = element.xpath(".//*")
        publications = element.xpath(".//publication|.//issue|.//article")
        target_child = None
        if publications:
            target_child = publications[0]
        elif children:
            target_child = children[0]
    except Exception as e:
        print(f"Error finding target child for {element.tag} {element.text}: {e}")
    return target_child

def gen_about_id(element: etree._Element, label:bool=False)->str:
    # Find deepest publication, volume, issue child of element and use its id, or generate an id from the text
    # children = element.xpath(".//*")
    # publications = element.xpath(".//publication|.//issue|.//article")
    target_child = get_about_target(element)
    # target_child_id = target_child.get('uri')
    # if publications:
    #     #print(f"Using id of deepest publication, volume or issue child of {element.tag} as id for {element} with text: {element.text}.")
    #     # If element has a uri attribute, use it as the id
    #     if target_child.get('uri'):
    #         element_id = clean_uri(target_child.get('uri'))
    #     else:
    #         element_id = gen_id(target_child)    # If no publication, volume or issue element is found, check for other children and use the first
    # elif children:
    #     if target_child.get('uri'):
    #         element_id = clean_uri(target_child.get('uri'))
    #         #print(f"Using child uri attribute as uri for {element}")
    #     else:
    #         element_id = gen_id(target_child)
    # elif element.text:
    #     element_id = gen_id_from_text(element)
    # else:
    #     print(f"Generated generic ID for {element}")
    #     element_id = gen_ad_label_id(element)

    # if target_child is not None:
    #     element.text = get_label(target_child)
    #print(f'Generated id {element_id} for:\n{to_string(element)}')
    element_id = gen_id(target_child, label)
    return element_id

def gen_duration_id(element: etree._Element, label:bool=False)->str:
    if not element.text.startswith('Issues'):
        element_label = f"Issues per {element.text}"
    else:
        element_label = element.text
    element_label = clean_label(element_label)
    element.text = element_label

    if label:
        element_id = element_label
    else:
        element_id = normalize(element_label)
    return element_id

def gen_time_span_id(element: etree._Element, label:bool=False)->str:
    element_label = 'Time-span'
    beginning = element.find('./beginning/date')
    end = element.find('./end/date')
    within = element.find('./within/date')
    if beginning is not None:
        element_label += f' beginning {beginning.text}'
    if end is not None:
        element_label += f' ending {end.text}'
    if within is not None:
        element_label += f' within {within.text}'
    element.text = clean_label(element_label)
    if label:
        element_id = element_label
    else:
        element_id = normalize(element_label)
    return element_id

def gen_value_id(element: etree._Element, label:bool=False)->str:
    value = element.text
    if not value == None:
        element_label = value
        currency = element.xpath("./ancestor::price/currency")
        if not currency == None:
            element_label += ' ' + currency[-1].text
    else:
        element_label = gen_ad_label_id(element)
        print(f"The element {str(element)} has no text. Generating generic ID.")

    element_label = clean_label(element_label)
    element.text = element_label
    if label:
        element_id = element_label
    else:
        element_id = normalize(element_label)
    return element_id

def gen_event_id(element: etree._Element, label:bool=False)->str:
    element_label = ''
    try:
        event_name = element.find('./event_name')
        event_type = element.find('./event_type')
        date = element.find('./date')
        timespan = element.find('./time_span')
        if event_name is not None:
            element_label = event_name.text
        elif event_type is not None:
            element_label = append_numeric_id(element, f"Unnamed {event_type.text}", label=True)

        if date is not None and event_name is None:
            date_text_element = date.find('./text')
            if date_text_element is not None:
                date_text = date_text_element.text
            else:
                date_text = date.text
            if date_text is None:
                date_text = 'unspecified date'
            #print(f"Time span: {time_span_text}")
            if event_type:
                element_label = f"{element_label} with {date.text}"
            else:
                element_label = f"Event with date {date_text}"
        elif timespan is not None:
            print(f"DATA ERROR: Event uses time span element. Use date element instead. Generating generic ID.")
            element_label = gen_ad_label_id(element, label=True)

        if not element_label:
            raise Exception("No event name, type, or date found")
    except Exception as e:
        print(f'Event {to_string(element)} is missing identifying information: {e}')
        element_label = gen_ad_label_id(element, label=True)

    element_label = clean_label(element_label)
    element.text = element_label
    if label:
        element_id = element_label
    else:
        element_id = normalize(element_label)
    return element_id

def gen_issuing_rule_id(element: etree._Element, label:bool=False)->str:
    no_issues = element.find('./no_issues')
    duration = element.find('./duration')
    element_label = f'Publish {no_issues.text} issues per {duration.text}'
    element_label = clean_label(element_label)
    element.text = element_label
    if label:
        element_id = element_label
    else:
        element_id = normalize(element_label)
    return element_id    

def gen_text_id(element: etree._Element, label:bool=False)->str:
    element_label = element.text
    if label:
        element_id = element_label
    else:
        element_id = gen_parent_tag_id(element)
        element_id = append_numeric_id(element, element_id, label)
    return element_id

def gen_parent_tag_id(element: etree._Element, label:bool=False)->str:
    parent = element.getparent()
    parent_label = gen_id(parent, True)
    element_label = f"{parent_label} - {element.tag}"
    element_label = append_numeric_id(element, element_label, label)
    if label:
        element_id = element_label
    else:
        element_id = normalize(element_label)
    return element_id

def gen_parent_subject_tag_id(element: etree._Element, label:bool=False)->str:
    subject = element.find('./about')
    ad = element.xpath('./ancestor::ad')[0]
    ad_label = gen_ad_id(ad, True)
    parent = element.getparent()
    parent_label = gen_id(parent, True)
    if not subject:
        subject = element.find('./subject')
    if subject is not None:
        element_label = f"{parent_label} - '{get_label(subject)}' {element.tag}"
        element_label = append_numeric_id(element, element_label, label)
    else:
        element_label = gen_ad_label_id(element, True)
        print(f"{element.tag} element from {ad_label} is missing subject. Generating generic ID.")
        print(f"Element: {to_string(element)}")

    if label:
        element_id = element_label
    else:
        element_id = normalize(element_label)
    #print(f'Generated parent subject tag ID {element_id} for {element.tag} {element.text}')
    if element.tag == 'form':
        print(f'Form id: {element_id}, label: {label}')

    return element_id

def gen_creation_event_id(element: etree._Element, label:bool=False)->str:
    publication = element.find('./publication')
    element_label = f"Creation of {get_label(publication)}"
    if label:
        element_id = element_label
    else:
        element_id = normalize(element_label)
    return element_id

def gen_date_id(element: etree._Element, label:bool=False)->str:
    date_text_element = element.find('./text')
    if element.text == None and date_text_element == None:
        #print(f"DATA ERROR: Date element {element.tag} has no text. Generating generic ID.")
        element_label = gen_ad_label_id(element, True)
    elif date_text_element == None:
        #print(f"DATA ERROR: Date text element not found for {element.tag} {element.text}. Appending text to new text element.")
        date_text_element = etree.Element('text')
        date_text_element.text = element.text
        element.append(date_text_element)
        #print(f"\tAppended text element: {to_string(date_text_element)}")
    if (not element.text == None) and ("date" in element.text.lower()):
        element_label = element.text
    else:
        parent = element.getparent()
        parent_tag = parent.tag
        #print(f"Parent tag: {parent_tag}")
        if parent_tag == 'date_founded':
            ancestor = parent.getparent()
            ancestor_label = gen_id(ancestor, True)
            element_label = f"Date founded of {ancestor_label}"
        elif (parent_tag == "beginning" or parent_tag == "end" or parent_tag == "within"):
            print(f"DATA ERROR: beginning/end/within elements should not have date elements as children. Generating generic ID.")
            element_label = gen_ad_label_id(element, True)
        elif parent_tag == 'about':
            element_label = f"Date of {date_text_element.text}"
        else:
            parent_label = gen_id(parent, True)
            element_label = f"Date of {parent_label}"
    #print(f"Date label: {element_label}")
    if label:
        element_id = element_label
    else:
        element_id = normalize(element_label)
    return element_id

about_tags = ['about', 'subject', 'represents', 'source']

def gen_id(element: etree._Element, label:bool=False)->str:
    element_id  = element.get('uri')
    element_text =  element.text
    if element_id and not label:
        element_id = clean_uri(element_id)
        #print(f'Element {element.tag} {element_text} already has an ID: {element_id}')           
    else:
        try:
            if (element.tag == 'copy') | (element.tag == 'list') | (element.tag == 'form') | (element.tag == 'price')| (element.tag == 'illustration'):
                element_label = gen_parent_tag_id(element, True)
                element.text = element_label
                if label:
                    element_id = element_label
                else:
                    element_id = normalize(element_label)
            elif (element.tag == 'name') | (element.tag == 'title') | (element.tag == 'event_name'):
                element_id = gen_id_from_text_and_tag(element, label)
            elif (element.tag == 'article') | (element.tag == 'publication'):
                element_id = gen_pub_id(element, label)
            elif (element.tag == 'issue') | (element.tag == 'volume'):
                element_id = gen_serial_id(element, label)
            elif (element.tag == 'ad') | (element.tag == 'embedded_ad'):
                element_id = gen_ad_id(element, label)
            elif (element.tag == 'creator') | (element.tag == 'author'):
                element_id = gen_creator_id(element, label)
            elif (element.tag == 'location'):
                element_id = gen_location_id(element, label)
            elif element.tag in about_tags:
                element_id = gen_about_id(element, label)
            elif (element.tag == 'duration'):
                element_id = gen_duration_id(element, label)
            elif (element.tag == 'time_span'):
                element_id = gen_time_span_id(element, label)
            elif (element.tag == 'date'):
                element_id = gen_date_id(element, label)
            elif (element.tag == 'event'):
                element_id = gen_event_id(element, label)
            elif (element.tag == 'issuing_rule'):
                element_id = gen_issuing_rule_id(element, label)
            elif (element.tag == 'text'):
                element_id = gen_text_id(element, label)
            elif (element.tag == 'creation_event'):
                element_id = gen_creation_event_id(element, label)
            elif ((element.tag == 'date_founded') |
                (element.tag == 'within') | (element.tag == 'beginning') | (element.tag == 'end')):
                element_id = gen_ad_label_id(element, label)
            else:
                element_id = gen_id_from_text(element, label)
        except Exception as e:
            print (f"\nAn error occured while generating a descriptive ID for {to_string(element)[0:100]}. Generating generic element ID. Error message:{e}")
            traceback.print_exc()
            element_id = gen_ad_label_id(element, label)
    if element_id == None:
        print(f"Error: Failed to generate id/label for element {to_string(element)[:100]}")
    #if label:
        #print(f'Generated label {element_id} for {element.tag} {element.text}')
    return element_id

def clean_uri(id:str)->str:
    # dictionary of prefixes and their associated URLs
    prefix_to_url = {
                'uuid': 'http://temp.lincsproject.ca/',
                'viaf': 'http://viaf.org/viaf/',
                'schema' : 'https://schema.org/',
                'locworks' : 'http://id.loc.gov/resources/works/',
                'geonames' : 'http://sws.geonames.org/',
                'bf' : 'http://id.loc.gov/ontologies/bibframe/',
                'owl' : 'http://www.w3.org/2002/07/owl#',
                'xsd' : 'http://www.w3.org/2001/XMLSchema#',
                'locprovs' : 'http://id.loc.gov/entities/providers/',
                'skos' : 'http://www.w3.org/2004/02/skos/core#',
                'rdfs' : 'http://www.w3.org/2000/01/rdf-schema#',
                'cwrc' : 'http://id.lincsproject.ca/cwrc#',
                'crmdig' : 'http://www.ics.forth.gr/isl/CRMdig/',
                'rso' : 'http://www.researchspace.org/ontology/',
                'locinsts' : 'http://id.loc.gov/resources/instances/',
                'aat' : 'http://vocab.getty.edu/aat/',
                'xml' : 'http://www.w3.org/XML/1998/namespace',
                'crm' : 'http://www.cidoc-crm.org/cidoc-crm/',
                'locsubjects' : 'http://id.loc.gov/authorities/subjects/',
                'fast' : 'http://id.worldcat.org/fast/',
                'locnames' : 'http://id.loc.gov/authorities/names/',
                'bibo' : 'http://purl.org/ontology/bibo/',
                'frbroo' : 'http://iflastandards.info/ns/fr/frbr/frbroo/',
                'wikidata' : 'http://www.wikidata.org/entity/',
                }
    if not id.startswith('http'):
        prefix, suffix = get_prefix_and_suffix(id)

        if prefix == 'uuid':
            print ('Predefined UUID found: ' + id)

        if prefix in prefix_to_url:
            prefix = prefix_to_url[prefix]
        else:
            print ('DATA ERROR: Unrecognized prefix: ' + prefix)
            prefix = 'http://temp.lincsproject.ca/'


        # Remove parentheses accidentally included in the URI (common copy-paste error)
        # Also remove zero-width spaces
        suffix = suffix.replace(')', '').replace('(', '').replace('\u200b', '')
        suffix = normalize(suffix, to_uppercase=False)

        id = prefix + suffix
    return id

def clean_label(label:str, to_titlecase:bool=False)->str:
    label = " ".join(label.replace('\n', " ").replace('_', " ").split())
    if to_titlecase:
        label = label.title()
    return label

def get_prefix_and_suffix(id:str)->str:
    if not id.startswith('http'):
        try:
            prefix = id.split(':')[0]
            suffix = id.split(':')[1]
        except:
            print(f"DATA ERROR: could not split {id} into prefix and suffix.")
            if id.startswith('Q'):
                prefix = 'wikidata'
                suffix = id
                print(f"\tAssuming {id} is a Wikidata URI. Assigning prefix {prefix}.")
            else:
                prefix = def_prefix
                suffix = id
                print(f"\tAssuming {id} is a local URI. Assigning prefix {prefix}.")
    prefix_suffix = [prefix, suffix]
    return prefix_suffix

def add_ids_to_tree(root:etree._Element)->None:
    for element in root.iter():
        uri = element.get('uri')
        # Remove extra whitespaces and newlines from element text
        # And get nested labels for about type elements
        element.text = clean_label(get_label(element))
        if not uri == None:
            # Add URIs from about elements to their children
            #TODO check if this is still necessary 
            if element.tag in about_tags:
                #print(f'About element found: {to_string(element)}')
                target = get_about_target(element)
                #print(f'About target: {to_string(target)}')
                target.set('uri', uri)
                if target.text == None and not element.text == None:
                    #print(f"Updating missing element text from about")
                    target.text = element.text
            # Remove OCLC uris, and instead add oclc id as an identifier element
            if uri.startswith('oclc:'):
                element.attrib.pop('uri')
                suffix = get_prefix_and_suffix(uri)[-1]
                uri = None
                identifier = etree.Element('identifier')
                identifier.text = suffix
                identifier_type = etree.Element('identifier_type')
                identifier_type.text = 'oclc'
                identifier_type.set('uri', 'http://id.loc.gov/vocabulary/identifiers/oclc')

                identifier.append(identifier_type)
                element.append(identifier)

        # Add IDs to elements that don't have them
        if uri == None:
            element_id = def_prefix + gen_id(element)
            try:
                element.set('uri', element_id)
            except:
                print(f'Cannot assign attribute to element \'{str(element)}\'')
        # Clean up existing IDs
        else:
            element_id = clean_uri(uri)
            element.set('uri', element_id)

def add_ids_to_file(input_file:os.PathLike, output_path:os.PathLike)->None:
    global tree
    global root
    global id_counts
    global issue_number
    tree = etree.parse(input_file, parser)
    root = tree.getroot()
    id_counts = {}
    issue_number = root.find('./issue/number').text

    add_ids_to_tree(root)

    input_file_name = input_file.split('/')[-1].split('.')[0]
    if input_file_name.startswith('issue'):
        output_file_name = f'{publication.lower()}_issue_{issue_number}_with_IDs.xml'
    else:
        output_file_name = f'{input_file_name}_with_IDs.xml'

    output_file = output_path + output_file_name

    print(f'Writing XML with IDs to {output_file}\n')
    with open(output_file, 'w') as file:
        file.write(etree.tostring(tree, encoding="unicode", pretty_print=True))

def add_ids_to_dir(dir_path:os.PathLike)->None:
    try:
        for file_name in os.listdir(dir_path):
            dir_path = Path(dir_path)
            f = os.path.join(dir_path, file_name)
            if os.path.isfile(f):
                if f.endswith('.xml'):
                    print(f'Adding ids to {f} ...')
                    add_ids_to_file(f, f'{dir_path.parent.absolute()}/with_ids/')
    # catch file not found errors
    except IOError as e:
        print(f'Error reading directory: {e}')

add_ids_to_dir(dir_path)
