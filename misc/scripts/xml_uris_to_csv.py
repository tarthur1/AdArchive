# Generates a csv of unique @uri attribute values from a directory of XML files
import csv
from lxml import etree
import os
import sys

global uris
uris = {}

parser = etree.XMLParser(remove_blank_text=True, remove_comments=True)

# Get source directory from argument
try:
    source_dir = sys.argv[1]
except:
    print('No source directory specified. Assuming current directory.')
    source_dir = '.'

# Add unique @uri values from an XML file to a dictionary
def add_uris_to_dict(xml_file_name):
    try:
        xml_file = open(f'{source_dir}/{xml_file_name}', 'r')
        tree = etree.parse(xml_file, parser)
        root = tree.getroot()
        for element in root.iter():
            if element.get('uri') is not None:
                uris[element.get('uri')] = xml_file_name
        return uris
    except Exception as e:
        print(f'Error parsing {xml_file}.\nError: {e}')

# Iterate through a directory of XML files and add unique @uri values to the dictionary
try:
    for file_name in os.listdir(source_dir):
        if file_name.endswith('.xml'):
            print(f'Processing {file_name}...')
            add_uris_to_dict(file_name)
except Exception as e:
    print(f'Error processing {file_name}.\nError: {e}')

# Write the dictionary to a csv file
print('Writing \'./misc/uris.csv\' ...')
try:
    with open('./misc/uris.csv', 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['uri', 'file'])
        for key, value in uris.items():
            writer.writerow([key, value])
except Exception as e:
    print(f'Error writing uris.csv.\nError: {e}')

