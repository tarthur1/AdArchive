# Monday, February 28, 2022

## Present: Michelle, Alex, Timothy

### To do:

- Email Kamal about implementing visualizations in website

- Complete git documentation

- Work on draft of XML documentation

# Thursday, March 3, 2022

## Present: Alex, Timothy

### To do:

* Check completed issues for issuing_rule

* Allow contents list to include creation event

* Allow linking author to type of content

* Content section with genre, 0-inf authors

* Multiple creators within ad_creators

# Monday, March 7, 2022

## Present: Jana, Michelle, Alex, Timothy

### To discuss:

- Little progress this week, under the weather

- Emailed Kamal

- Met with Alex, determined some further changes to make to the schema

- Planning goals and roles for final two months
  
  - Priorities:
    
    1. Finalize schema 1.0
    
    2. Finish 3M mapping of schema 1.0
       
       - Check with Erin re. potential changes to application profile
    
    3. Ready documentation for transition 
       
       1. Git - Me
       
       2. XML authoring - Alex?
       
       3. 3M conversion - Me
       
       4. Schema revision - Me
       
       5. Reconciliation
          
          - Plan reconciliation workflow
          - Reconcile completed issues
          - Create reconciliation documentation
       
       6. Further data authoring

### To do:

- Update schema

- Documentation
  
  - Getting started: Oxygen and git
  - Data authoring

- Email Natalie, Zachary, Erin to inquire about issues 9 and 10
  
  - How far along? 
  - Should we convert from n-triples or enter into xml schema

- Work on mapping

- State of the project
  
  - for Jana and Michelle

# Monday, March 14, 2022

## Present:  Jana, Michelle, Alex, Timothy

## Completed this week:

- Schema update

## To do:

- 3m mapping   

- Git documentation
  
  - Update to include branch management
  
  - Integrate into Oxygen documentation

- Look into oxygen global settings, for incorporation in the documentation

- Keep ‘state of the project’ statement in mind
  
  - Send this to LINCS team rather than inquiring separately about issues 9 and 10

- Bio for website

# Monday, March 21, 2022

## Present: Michelle, Jana, Timothy

## Completed this week:

- Met with Alex, discussed Git workflow, specific schema elements and documentation

- Have 20 hours to catch up before the end of April

- Should be able to start making up time in the coming weeks as I am over the busiest spell

### To do:

- 3m mapping   

- Git documentation
  
  - Update to include branch management
  
  - Integrate into Oxygen (getting started) documentation

- Look into oxygen global settings, for incorporation in the documentation

- Keep ‘state of the project’ statement in mind
  
  - Send this to LINCS team rather than inquiring separately about issues 9 and 10

- Bio for website

- Review completed issues

# Monday April 25:

## Present: Alex, Michelle, Jana, Me

## Work completed:

- 3M mapping
  
  - Switched to new CIDOC version
  
  - 80% done
  
  - Some questions for Erin
  
  - Will complete the rest this week

- Made some small schema changes (mostly additions)
  
  - Should meet with Alex on Thursday to discuss

- Prepare for meeting with Kamal
  
  - Installed visualization software
  
  - Will test prior to meeting

## To do:

- Insert source links into glossary

- 3M mapping
  
  - Finish draft
  
  - Contact Erin with questions

- Meet with Alex re schema changes

- Meet with Kamal

# Wednesday, April 27:

## LINCS Meeting Re. 3M mapping

## Present: Me, Jana, Michelle, Erin, Natalie, Kim

## Update:

- Work on the 3M mapping was sidelined for a while

- Was working on creating a schema that would encompass the breadth of our data:
  
  - Included training Alex on authoring data using the schema
  
  - entering data for issues 
  
  - iteratively making schema adjustments
  
  - We now have about 10 issues authored using the xml schema

- Schema has become more stable, so I have embarked again on creating the 3M mapping
  
  - Nearly complete

## 3M mapping to do:

- Address questions posed in document

- Copy nested components

- Create custom label generators for entities

- Reconciliation

# Thursday, April 28:

## Meeting with Alex

## Discuss:

- Schema changes

- Contents list usage

## To do:

- Update latest complete issue to new schema as example and walk through at next meeting

# Monday, May 9:

## Weekly meeting

## Present: Me, Michelle, Jana

## Report:

- Working on mapping based on Erin's answers to my questions
  
  - Will complete this week and send to Erin and Natalie

- Added links for cidoc sources and relevant application profile sections in glossary
  
  - Will add wikidata type links

- Created an example dataset for website visualization

## To do:

- Complete mapping

- Add wikidata links to glossary

- Meet with Alex re. latest schema

- Play around with graph visualization

- Try setting up carousel on website

# Monday, July 18, 2022:

## Weekly meeting

## Present: Michelle, Jana, Me

## Report:

- Erin updated LINCS model for our data
  
  - Simplified, relieves some confusion about publication types
    
    > 1. Instead of using the pattern <crm:E73_Information_Object crm:P102_has_title crm:E35_Title> to state that an object can be identified by a set of words, we are using <crm:E73_Information_Object crm:P1_is_identified_by crm:E33_E41_Linguistic_Appellation crm:P2_has_type “Title”> . This allows us to look at all ways that objects are identified by sets of words, while also retaining the specificity that this specific set of words is a title. 
    > 
    > 2. Correct Prices modeling and update to use 7.1.1 “price” classes and properties: 
    >    
    >    1. Changing E16_Measurement [prices] to E13_Attribute_Assignment
    >    
    >    2. Changing P39_measured [prices] to P140_assigned_attribute_to
    >    
    >    3. Changing P39i_was_measured_by [prices] to P140i_was_attributed_by
    >    
    >    4. Changing P40_observed [prices] to P141_assigned
    >    
    >    5. Changing E54_Dimension [prices] to E97_Monetary_Amount
    >    
    >    6. Changing P91_has_unit [prices] to P180_has_currency
    >    
    >    7. Changing E58_Measurement_Unit [prices] to E98_Currency
    > 
    > 3. FRBRoo-related changes:
    >    
    >    1. Changing classes frbroo:F18_Serial_Work, frbroo:F22_Publication_Expression, frbroo:F3_Manifestation, and frbroo:F4_Manifestation_Singleton to frbroo:F2_Expression
    >    
    >    2. Changing classes frbroo:F28_Expression_Creation and frbroo:F30_Publication_Expression to crm:E65_Creation
    >    
    >    3. Changing properties frbroo:R19i_was_realized_through and frbroo:R24i_was_created_through to crm:P94i_was_created_in
    >    
    >    4. Changing properties frbroo:R10_has_member/frbroo:R10i_is_member_of and frbroo:R67_has_part/frbroo:R67i_forms_part_of to crm:P148_has_component/crm:P148i_is_component_of
    > 
    > 4. New
    >    
    >    1. Adding crm:P76_has_contact_point between actor and address, phone number
    >    
    >    2. Changing undocumented pattern <frbroo:F2_Expression frbroo:R11_has_issuing_rule crm:E29_Design_or_Procedure> to <frbroo:F2_Expression crm:P94i_was_created_in crm:E65_Creation crm:P33_used_specific_technique crm:E29_Design_or_Procedure>

- Updated schema based on new data model and group review
  
  - serial_advertised merged with publication_advertised, since all publications are now treated as Expressions
    
    - Wikidata types already in our data used to differentiate between types of publication
  
  - Generalized subscription_form to "form" to encompass other types of forms (e.g. order forms)
    
    - Need to change this in the existing data and insert form types

- Tested the mapping on real data from issue 3
  
  - found that 3M would sometimes mix information from different ads, e.g. copy from one ad would be linked to a the wrong ad
    
    - Consulted Erin about this
    
    - Will have to assign unique identifiers to each schema element
    
    - Will write a script that does this prior to conversion to RDF

To do:

- Maintain list of schema elements for new student review

- Documentation
  
  - Review and supplement Alex's glossary entries

# Monday, July 25, 2022

## Present: Alex, Michelle, Jana, Me

## Report:

- Fixed ad uniqueness problem by generating identifiers for all unique elements without existing URIs

- Working on mapping
  
  - Goal is to report to Erin this week on remaining modeling or mapping questions

# Monday, January 23, 2023

## Report:

- Spent last week on getting IIIF clipping working and running install and training sessions

- Looked this morning into what will be required to generate and host new IIIF images for new publications:
  
  - Was not able to find Sinister Wisdom 11 on Internet Archive
  
  - It seems that IA resources with access restrictions (ones you have to check out) are not available via their own IIIF hosting
  
  - We have a few options for hosting new IIIF images:
    
    - Host on our website or other university website
      
      - Contact library to see if they are doing any IIIF hosting of their own
      
      - Contact ARC re hosting on our own website
    
    - Generate less dynamic IIIF images (level 0) and host on gitlab or our own website
    
    - Pay for hosting service (9 - 70 USD per year)

# Friday, February 17, 2023

## Meeting on LINCS conference

- How can a feminist orientation show the constructedness and incompleteness of the data?

- How are we representing constructedness and incompleteness?

- Haunting, ghost in the machine, perennial marginalization

- 
